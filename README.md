# ChicoServo - HW #


This is a fully working board which you may wanna use as reference or may clone and customize to your needs. The board was designed to an specific project which required ADC, PWM, SERIAL, and I/O connection.

Example Firmware [**Here**](https://bitbucket.org/jpnbino/fw_palhaservo/src/master/).

![ChicoServo](https://lh3.googleusercontent.com/-xAzo6do1YXg/W6qMOTh-vUI/AAAAAAAAHn0/Q8DBVWLUJSYrY31MveT1Ac76fYBifR78QCJoC/w530-h380-n-rw/chicoservo_sem_fundo.png)



### Board details ###

|  Item           |Description |
|-----------------|------------|
| Core            | PIC16LF1824|
| Input Voltage (Vin)   |  4.75V ≤ Vin ≤ 10V    |
| Board Size      | 16.5mm x 31mm|
| User LED        | 1            |

### The project ###

* Quick summary

    + Contains source files from IDE Altium Designer which can be used rebuild this project or adopted as an user may need.
    
* Version

    + V1I1 - Initial.

### Video ####
I recorded this video when I was building the first prototype.

[![Imgur](https://i.imgur.com/e7VBtIf.png)](https://youtu.be/o7HVy0H5WR8)

### Who do I talk to? ###

* Any questions? Just give me a call
        [jpnbino@gmail.com](jpnbino@gmail.com)